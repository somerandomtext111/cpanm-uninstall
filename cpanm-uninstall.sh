#!/bin/sh

VERSION=1.1
usage_exit=1

USAGE () {
  cat <<EOF
Usage: $(basename $0) [-d] [-y] LOG
       $(basename $0) [-h --help] [-v --version]

Uninstall Perl modules previously installed with cpanm.
A build.log of the operation is required. An STDOUT dump of the installation process works too.
You have neither - you're out of luck.

Arguments:
  LOG    build.log or an installation output dump file

Parameters:
  -d               Dry run - show what's about to be done but don't actually delete anything
  -y               Assume 'y' (yes) for all uninstall prompts
  -h, --help       Print this help and exit
  -v, --version    Print current version and exit
EOF
  exit $usage_exit
}

VERSION () {
  echo "$(basename $0) ver. $VERSION"
  exit 0
}

info () {
  echo "* $@"
}

[ -z "$1" ] && USAGE

case "$1" in
  -h|--help) usage_exit=0; USAGE ;;
  -v|--version) VERSION ;;
esac

dry_run=false
assume_yes=false

while getopts dy OPT 2>/dev/null; do
  case "$OPT" in
    d) dry_run=true ;;
    y) assume_yes=true ;;
    *) USAGE ;;
  esac
done

shift $((OPTIND - 1))

[ -z "$1" ] && USAGE

info "Grepping log at $1"

success=$([ -z "$(grep -ie failed $1)" ] && echo 1)
testing=$([ -n "$(grep 'Successfully tested' $1)" ] && echo 1) # cpanm was invoked with --test-only (which means installing all dependencies crap anyway and never cleaning it up)
modules=$(<"$1" perl -nswE '(undef, $a) = /Successfully (\w+)ed (.+)$/ and $a =~ s/\-[^-]+$// and $a =~ s/\-/::/g; say $a if $a')
status=$? && [ $status -gt 0 ] && exit $status

[ -n "$testing" -a -n "$success" ] && modules=$(echo $modules | perl -pnswe 's/\s[^\s]+$//') # exclude the last module from the candidates list

info "About to uninstall the following modules:"
echo $modules

$dry_run && exit 0

$assume_yes && {
  stdin=

  for i in $modules; do
    stdin="${stdin}y\n"
  done

  echo -e $stdin | cpanm -U $modules
} || {
  cpanm -U $modules
}
