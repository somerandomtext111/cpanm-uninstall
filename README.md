## cpanm-uninstall.sh

Ever wondered if there's an easy way to undo `cpanm something` including all dependencies it brings along? Well, now there is!

## Usage

```sh
cpanm-uninstall.sh --help
```

## Copyright

Copyright (c) 2024 Vasyan

## Version

1.1
